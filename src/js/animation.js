"use strict";
~(function() {
    var tl,
        bgExit = document.getElementById("bgExit");
      

    window.init = function() {
        bgExit.addEventListener("click", bgExitHandler);
        tl = new TimelineMax({});       

        tl.to("#copyOne", 1, { x: 0,y:0, ease: Power2.easeInOut }, "+=1");
        tl.to("#products", 1, { x: 0, y: 0, ease: Power2.easeInOut }, "-=1");

        tl.to("#copyOne", 1, { opacity:0, ease: Power2.easeInOut }, "+=1");
        tl.to("#copyTwo", 1, { x: 0, y: 0, ease: Power2.easeInOut },"-=0.5");

        tl.to("#cta", 1, { opacity:1, ease: Power2.easeInOut }, "-=0.5");       
        

    };


    function bgExitHandler(e) {
        e.preventDefault();
        window.open(window.clickTag);
    }
})();